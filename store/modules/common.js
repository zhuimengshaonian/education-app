export default {
	
  namespaced: true,
  state: {
     host: 'http://120.79.144.34:8003', //'http://education.free.idcfengye.com',//'http://127.0.0.1', //,
	 fileHost: 'https://education-prod-1253719016.cos.ap-nanjing.myqcloud.com'
  },
  
  getters: {
	  
	   host: state => {
	      return state.host
	   },
	   
	   fileHost: state => {
	      return state.fileHost
	   },
  },
  
  mutations: {

  }
}
